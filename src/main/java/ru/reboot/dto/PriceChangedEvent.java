package ru.reboot.dto;

/**
 * Price changed event.
 * Use in PRICE_CHANGED_EVENT kafka topic
 */
public class PriceChangedEvent {

    public static final String TOPIC = "PRICE_CHANGED_EVENT";

    /**
     * Event type
     */
    public static class Type {
        public static final String UPDATE = "UPDATE";
        public static final String DELETE = "DELETE";
    }

    private PriceInfo price;
    private String eventType;

    public PriceChangedEvent() {
    }

    public PriceChangedEvent(String eventType, PriceInfo price) {
        this.price = price;
        this.eventType = eventType;
    }

    public PriceInfo getPrice() {
        return price;
    }

    public void setPrice(PriceInfo price) {
        this.price = price;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    @Override
    public String toString() {
        return "PriceChangedEvent{" +
                "eventType=" + eventType +
                " price=" + price +
                '}';
    }
}
