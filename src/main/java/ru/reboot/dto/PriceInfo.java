package ru.reboot.dto;

public class PriceInfo {

    public PriceInfo() {
    }

    public PriceInfo(String itemId, double price) {
        this.itemId = itemId;
        this.price = price;
    }

    private String itemId;
    private double price;

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "PriceInfo{" +
                "itemId='" + itemId + '\'' +
                ", price=" + price +
                '}';
    }
}
