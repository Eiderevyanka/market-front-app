package ru.reboot.dto;

import java.util.HashMap;
import java.util.Map;

public class CategoryInfo {

    private final Map<String, String> NAMES = new HashMap<>();

    {
        NAMES.put("FRUITS", "Фрукты");
        NAMES.put("VEGETABLES", "Овощи");
        NAMES.put("CHEESES", "Сыры");
        NAMES.put("CANDIES", "Конфеты");
        NAMES.put("SAUSAGES", "Колбасы");
        NAMES.put("CANNEDFOOD", "Консервы");
        NAMES.put("BAKERY", "Хлебобулочные изделия");
        NAMES.put("ALCOHOL", "Алкоголь");
    }

    private String id;
    private String name;

    public CategoryInfo(String id) {
        this.id = id;
        this.name = NAMES.getOrDefault(id, "UNKNOWN_CATEGORY_NAME (ID=" + id + ")");
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "CategoryInfo{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
