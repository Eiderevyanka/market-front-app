let categories = [];
let items = [];
let orders = [];
let basketItems = new Map;

function getCategories() {
    let result = [];
    let xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            result = JSON.parse(this.responseText);
        }
    };
    xhttp.open("GET", "/market/category/all", false);
    xhttp.send();
    return result;
}

function getItems(category) {
    let xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            items = JSON.parse(this.responseText);
        }
    };
    xhttp.open("GET", "/market/item/all/byCategory?categoryId=" + category, false);
    xhttp.send();
    return items;
}

function showMainPage() {
    categories = getCategories();

    let mainPage = "<button class='greenButton' onclick='showMyOrdersPage()'>Мои заказы</button>" +
        "<button class='greenButton' onclick='showBasket()'>Корзина</button>" +
        "<h1 class='main'>Welcome to SMarket</h1><div class='root'>"

    for (let i = 0; i < categories.length; i++) {

        let categoryId = categories[i].id;

        mainPage += "<div id='" + categoryId + "' class='category'><p class='category'>" + categories[i].name + "</p>" +
            "<img id='" + categoryId + "Image' src='images/" + categoryId.toLowerCase() + ".jpg' alt='image'></div>"
    }

    mainPage += "</div>"

    document.getElementById("root").innerHTML = mainPage

    for (let i = 0; i < categories.length; i++) {
        document.getElementById(categories[i].id).addEventListener("click", onCategoryClicked)
        document.getElementById(categories[i].id + 'Image').addEventListener("click", onCategoryClicked)
    }
}

function showCategoryPage(category) {

    items = getItems(category.id);
    let mainPage = "<button class='greenButton' onclick='showMyOrdersPage()'>Мои заказы</button>" +
        "<button class='greenButton' onclick='showBasket()'>Корзина</button>" +
        "<h1 class='main'>" + category.name + "</h1>"

    for (let i = 0; i < items.length; i++) {
        mainPage += "<div class='big'><div class='item'><p class='short'>" + items[i].name +
            "</p><p class='price'>Цена: " + items[i].price + "  руб. за " + items[i].unit + "</p>" +
            "<p class='price'>Страна производитель: " + items[i].manufacturer + "</p></div>" +
            "<button id='" + items[i].id + "' class='info'>Подробнее</button></div>"
    }

    mainPage += "<br><button class='link' onclick='showMainPage()'>Назад</button>"
    document.getElementById("root").innerHTML = mainPage

    for (let i = 0; i < items.length; i++) {
        document.getElementById(items[i].id).addEventListener("click", onItemClicked)
    }
}

function showItemPage(item) {
    let mainPage = "<button class='greenButton' onclick='showMyOrdersPage()'>Мои заказы</button>" +
        "<button class='greenButton' onclick='showBasket()'>Корзина</button>" +
        "<div class='itemInfo'><h1 class='itemInfo'>" + item.shortName + "</h1>" +
        "<p class='itemInfo'>Название: " + item.name + "</p>" +
        "<p class='itemInfo'>Описание: " + item.description + "</p>" +
        "<p class='itemInfo'>Единица измерения: " + item.unit + "</p>" +
        "<p class='itemInfo'>Цена: " + item.price + "</p>" +
        "<p class='itemInfo'>Производитель: " + item.manufacturer + "</p>" +
        "<p class='itemInfo'>Остаток: " + (item.count - item.reserved) + "</p>" +

        "<p class='itemInfo'>" +
        "<div class='count'>" +
        " <table class='countTable'>" +
        "<tr>" +
        "<th>" +
        "<button class='minus'>-</button>" +
        "</th>" +
        "<th>" +
        "<div class='number'>0</div>" +
        "</th>" +
        "<th>" +
        "<button class='plus'>+</button>" +
        "</th>" +
        "<th>" +
        "<button class='addItem'>Добавить в корзину</button>" +
        "</th>" +
        "</tr>" +
        "</table>" +
        "</div></p>" +

        "</div>"
    mainPage += "<br><button id='" + item.category + "' class='link'>Назад</button>"
    document.getElementById("root").innerHTML = mainPage
    document.getElementById(item.category).addEventListener("click", onCategoryClicked)

    if (basketItems.has(item.id)) {
        document.querySelector(".number").innerHTML = basketItems.get(item.id)[1]
    }

    function addHandlers(count) {
        let minus = count.querySelector(".minus");
        let number = count.querySelector(".number");
        let plus = count.querySelector(".plus");
        let addItem = count.querySelector(".addItem");
        plus.addEventListener("click", function () {
            if (number.textContent == (item.count - item.reserved)) {
                number.textContent = (item.count - item.reserved)
            } else {
                number.innerText++;
            }
        });
        minus.addEventListener("click", function () {
            if (number.textContent == 0) {
                number.textContent = 0
            } else {
                number.innerText--;
            }
        });
        addItem.addEventListener("click", function () {

            if (number.innerHTML != 0) {
                basketItems.set(item.id, [item, number.innerHTML])
                alert('Товар добавлен в корзину')
            } else if (basketItems.has(item.id)) {
                basketItems.delete(item.id)
                alert('Товар удалён из корзины')
            } else {
                alert('Выберите необходимое количество товара')
            }
        })
    }

    let counts = document.querySelectorAll(".count");
    counts.forEach(addHandlers);
}

function onItemClicked(event) {

    let item = items.find(item => item.id == event.target.id);
    showItemPage(item)
}

function onCategoryClicked(event) {
    let category;
    if (event.target.id.includes('Image')) {
        category = categories.find(cat => cat.id == event.target.id.slice(0, event.target.id.indexOf('Image')));
    } else {
        category = categories.find(cat => cat.id == event.target.id);
    }
    showCategoryPage(category)
}

function showMyOrdersPage() {
    let mainPage = "<div class='myOrders'><h1>Мои заказы</h1>";

    mainPage += "<form>"
        + "<table id='ordersTable'><tr>"
        + "<td>Телефон</td>"
        + "<td><input id='phoneNumber' type='tel' name='phoneNumber' required></td>"
        + "<td><input class='greenButton' type='button' value='Отправить' onclick='showTable()'></td>"
        + "</tr></table>"
        + "</form>"

        + "<div id='orders'>"
        + "</div>"

        + "<br><button class='link' onclick='showMainPage()'>Назад</button>"
        + "</div>";

    document.getElementById("root").innerHTML = mainPage;

}

function validatePhoneNumber() {

    let phoneNumber = document.getElementById('phoneNumber').value;

    if (isNaN(phoneNumber) || phoneNumber < 0 || phoneNumber.length !== 10) {
        alert('Необходимо ввести 10-ти значный номер телефона')
        document.getElementById('phoneNumber').style.color = "red"
        document.getElementById('phoneNumber').style.background = "LightYellow"
        return false
    }
    document.getElementById('phoneNumber').style.color = ""
    document.getElementById('phoneNumber').style.background = ""
    return true
}

function showTable() {

    if (!validatePhoneNumber()) {
        return
    }

    let http = new XMLHttpRequest();
    let url = '/market/orders/byPhoneNumber?phoneNumber=' + document.getElementById('phoneNumber').value;
    http.open('GET', url, false);
    http.send();

    if (http.status === 200) {
        orders = JSON.parse(http.responseText);
        if (orders.length > 0) {

            let table = "<table id='ordersByPhoneTable'>"
                + "<tr>"
                + "<th>№</th>"
                + "<th>Адрес доставки</th>"
                + "<th>Дата доставки</th>"
                + "<th>Количество покупок</th>"
                + "<th></th>"
                + "</tr>"

            for (let i = 0; i < orders.length; ++i) {
                table += createRow(i + 1, orders[i])
            }

            table += "</table>"

            document.getElementById('orders').innerHTML = table
        } else {
            alert('По данному номеру телефона заказов не найдено')
            showMyOrdersPage()
        }
    } else if (http.status === 500) {
        JSON.parse(http.responseText, function (key, value) {
            if (key === 'message') {
                alert(value)
            }
        })
    } else {
        alert('Произошла ошибка')
    }
}

function createRow(idx, order) {

    let items = 0
    for (let item in order.items) {
        items++
    }

    return "<tr>"
        + "<td class='orderTable'>" + idx + "</td>"
        + "<td class='orderTable'>" + order.address + "</td>"
        + "<td class='orderTable'>" + order.deliveryDate + "</td>"
        + "<td class='orderTable'>" + items + "</td>"
        + "<td><button class='greenButton' onclick=deleteOrder('" + order.orderId + "')>Удалить</button></td>"
        + "</tr>"
}

function deleteOrder(orderId) {

    let http = new XMLHttpRequest();
    let url = '/market/order?orderId=' + orderId;
    http.open('DELETE', url, false);
    http.send();

    if (http.readyState === 4 && http.status !== 200) {
        alert('Произошла ошибка при удалении заказа')
    } else {
        showTable()
    }
}

function showBasket() {
    let mainPage = "<div class='basket'><h1>Корзина</h1>"
    if (basketItems.size !== 0) {
        mainPage += "<table id='orderTable'>"
            + "<tr>"
            + "<th>Название товара</th>"
            + "<th>Количество</th>"
            + "<th>Сумма</th>"
            + "<th></th>"
            + "</tr>"

        let sum = 0;
        for (const [key, value] of basketItems) {
            mainPage += "<tr>" +
                "<td class='orderTable'>" + value[0].name + "</td>" +
                "<td class='orderTable'>" + value[1] + "</td>" +
                "<td class='orderTable'>" + (value[0].price * value[1]) + "</td>" +
                "<td><button class='greenButton' onclick=deleteItem('" + key + "')>Удалить</button></td>" +
                "</tr>"
            sum += (value[0].price * value[1])
        }

        mainPage += "</table>" +
            "<table id='sumTable'>" +
            "<tr>" +
            "<th>Сумма заказа</th>" +
            "<th>" + sum + "</th>" +
            "</tr>" +
            "<td><button class='greenButton' id='delAllFromBasket'>Удалить товары из корзины</button></td>" +
            "<td><button class='greenButton' onclick='showOrderPage(" + sum + ")'>Заказать</button></td>" +
            "</table>"
    } else {
        mainPage += "<p>Товары не выбраны</p>"
    }

    mainPage += "<br><button class='link' onclick='showMainPage()'>Назад</button>"
    document.getElementById("root").innerHTML = mainPage;
    if (basketItems.size !== 0) {
        document.getElementById("delAllFromBasket").addEventListener("click", function () {
            basketItems.clear()
            showBasket()
        })
    }
}

function deleteItem(key) {
    basketItems.delete(key)
    showBasket()
}


function showOrderPage(sum) {
    let mainPage = "<div class='basket'><h1>Оформление заказа</h1>"
        + "<table id='orderPage'>"
        + "<tr>"
        + "<td>Адрес доставки</td>"
        + "<td><input id='address' value='' color = 'black' required></td>"
        + "</tr>"

        + "<tr>"
        + "<td>Дата доставки</td>"
        + "<td><input id='deliveryDate' type='datetime-local' color = 'black' required></td>"
        + "</tr>"

        + "<tr>"
        + "<td>Сумма заказа</td>"
        + "<td><input id='orderSum' value=" + sum + " readonly></td>"
        + "</tr>"

        + "<tr>"
        + "<td>Телефон</td>"
        + "<td><input id='phoneNumber' type='tel' value='' color = 'black' required></td>"
        + "</tr>"

        + "<tr>"
        + "<td></td>"
        + "<td>"
        + "<button class='darkGreenButton' onclick='onCreateOrderButtonClicked()'>Заказать</button>"
        + "</td>"
        + "</table>"

    mainPage += "<br><button class='link' onclick='showBasket()'>Назад</button>"
    document.getElementById("root").innerHTML = mainPage;
}

function onCreateOrderButtonClicked() {

    if (!validateOrder()) {
        return
    }

    let orderItems = {};
    for (const [key, value] of basketItems) {
        orderItems[key] = value[1]
    }

    let order = {
        orderId: Date.now(),
        phoneNumber: document.getElementById('phoneNumber').value,
        address: document.getElementById('address').value,
        orderDate: null,
        deliveryDate: document.getElementById('deliveryDate').value,
        items: orderItems,
    }

    let orderJSON = JSON.stringify(order);
    alert('Подтвердите создание заказа')

    let http = new XMLHttpRequest()
    http.open('POST', '/market/order', false)
    http.setRequestHeader('Content-Type', 'application/json');
    http.send(orderJSON)

    if (http.status === 200) {
        alert('Заказ успешно создан')
        basketItems = new Map
        showMainPage()
    } else if (http.status === 500) {
        JSON.parse(http.responseText, function (key, value) {
            if (key === 'message') {
                alert(value)
            }
        })
    } else {
        alert('Произошла ошибка при создании заказа')
    }
}

function validateOrder() {

    let hasEmptyFields = true
    let params = {
        address: '',
        deliveryDate: '',
        phoneNumber: '',
    }
    for (let param in params) {
        if (document.getElementById(param).value === '') {
            document.getElementById(param).placeholder = 'Заполните это поле'
            hasEmptyFields = false
        }
    }
    if (!hasEmptyFields) {
        alert("Необходимо заполнить все поля")
        return false
    }
    return validatePhoneNumber();
}

