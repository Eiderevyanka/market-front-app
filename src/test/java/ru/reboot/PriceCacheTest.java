package ru.reboot;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import ru.reboot.dto.PriceInfo;
import ru.reboot.error.BusinessLogicException;
import ru.reboot.error.ErrorCode;
import ru.reboot.service.PriceCache;


import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

import static org.mockito.Mockito.doAnswer;

public class PriceCacheTest {

    private final PriceInfo priceInfo1 = new PriceInfo("tea", 50.0);

    private final PriceInfo priceInfo2 = new PriceInfo("lipton", 12.0);

    private final PriceInfo priceInfo3 = new PriceInfo("candy", 120.0);

    private final Map<String, PriceInfo> priceInfoMap = new HashMap<>();

    {
        priceInfoMap.put(priceInfo1.getItemId(), priceInfo1);
        priceInfoMap.put(priceInfo2.getItemId(), priceInfo2);
    }

    @Test
    public void getWhenReturnPrice_Test() {

        PriceCache priceCacheMock = Mockito.mock(PriceCache.class);
        Mockito.when(priceCacheMock.get(priceInfo1.getItemId())).thenReturn(priceInfo1.getPrice());

        Double result = priceCacheMock.get("tea");
        Assert.assertEquals(priceInfo1.getPrice(), result);

        Mockito.when(priceCacheMock.get(priceInfo2.getItemId())).thenReturn(priceInfo2.getPrice());

        Double result2 = priceCacheMock.get("lipton");
        Assert.assertEquals(priceInfo2.getPrice(), result2);

        Assert.assertNotEquals(priceInfo1.getPrice(), result2);

    }

    @Test
    public void getWhenReturnNull_Test() {

        PriceCache priceCacheMock = Mockito.mock(PriceCache.class);
        Mockito.when(priceCacheMock.get(priceInfo3.getItemId())).thenReturn(null);

        Double result = priceCacheMock.get("candy");

        Assert.assertNull(result);
        Assert.assertNotEquals(priceInfo3.getPrice(), result);
        Assert.assertEquals(null, result);

    }

    @Test
    public void getWhenThrowException_Test() {

        PriceCache priceCacheMock = Mockito.mock(PriceCache.class);
        Mockito.when(priceCacheMock.get(null)).thenThrow(new BusinessLogicException("itemId can't be null", ErrorCode.ILLEGAL_ARGUMENT));

        try {
            priceCacheMock.get(null);
            Assert.fail();
        } catch (BusinessLogicException e) {
            Assert.assertEquals(e.getCode(), "ILLEGAL_ARGUMENT");
        }
    }

    @Test
    public void containsWhenReturnTrue_Test() {

        PriceCache priceCacheMock = Mockito.mock(PriceCache.class);
        Mockito.when(priceCacheMock.contains(priceInfo1.getItemId())).thenReturn(true);

        Boolean result = priceCacheMock.contains("tea");

        Mockito.when(priceCacheMock.contains(priceInfo2.getItemId())).thenReturn(true);

        Boolean result2 = priceCacheMock.contains("lipton");

        Assert.assertEquals(true, result);
        Assert.assertNotEquals(false, result);
        Assert.assertEquals(true, result2);
        Assert.assertNotEquals(false, result2);
    }

    @Test
    public void containsWhenReturnFalse_Test() {

        PriceCache priceCacheMock = Mockito.mock(PriceCache.class);
        Mockito.when(priceCacheMock.contains(priceInfo3.getItemId())).thenReturn(false);

        Boolean result = priceCacheMock.contains("candy");

        Assert.assertEquals(false, result);
        Assert.assertNotEquals(true, result);
    }

    @Test
    public void containsWhenThrowException_Test() {

        PriceCache priceCacheMock = Mockito.mock(PriceCache.class);
        Mockito.when(priceCacheMock.contains("")).thenThrow(new BusinessLogicException("itemId can't be null", ErrorCode.ILLEGAL_ARGUMENT));

        try {
            priceCacheMock.contains("");
            Assert.fail();
        } catch (BusinessLogicException e) {
            Assert.assertEquals(e.getCode(), "ILLEGAL_ARGUMENT");
        }
    }

    @Test
    public void putAndDeletePositive_Test() {
        PriceCache priceCacheMock = new PriceCache();

        priceCacheMock.put("candy", priceInfo3);

        Assert.assertEquals(priceInfo3.getPrice(), priceCacheMock.get(priceInfo3.getItemId()));
        Assert.assertEquals(true, priceCacheMock.contains(priceInfo3.getItemId()));

        priceCacheMock.delete("candy");

        Assert.assertEquals(null, priceCacheMock.get(priceInfo3.getItemId()));
        Assert.assertEquals(false, priceCacheMock.contains(priceInfo3.getItemId()));

    }
}