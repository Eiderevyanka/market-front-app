package ru.reboot.dto;

public class ItemInfo {

    private String id;
    private String name;
    private String shortName;
    private String description;
    private String unit;
    private double purchasePrice;
    private double price;
    private String category;
    private String manufacturer;
    private int count;
    private int reserved;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getPurchasePrice() {
        return purchasePrice;
    }

    public void setPurchasePrice(double purchasePrice) {
        this.purchasePrice = purchasePrice;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getReserved() {
        return reserved;
    }

    public void setReserved(int reserved) {
        this.reserved = reserved;
    }

    public ItemInfo(ItemDTO itemDTO, double price) {
        this.id = itemDTO.getId();
        this.name = itemDTO.getName();
        this.shortName = itemDTO.getShortName();
        this.description = itemDTO.getDescription();
        this.unit = itemDTO.getUnit();
        this.price = price;
        this.purchasePrice = itemDTO.getPurchasePrice();
        this.category = itemDTO.getCategory();
        this.manufacturer = itemDTO.getManufacturer();
        this.count = itemDTO.getCount();
        this.reserved = itemDTO.getReserved();
    }

    @Override
    public String toString() {
        return "ItemSale{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", shortName='" + shortName + '\'' +
                ", description='" + description + '\'' +
                ", unit='" + unit + '\'' +
                ", price=" + price +
                ", category='" + category + '\'' +
                ", manufacturer='" + manufacturer + '\'' +
                ", count=" + count +
                ", reserved=" + reserved +
                '}';
    }
}
