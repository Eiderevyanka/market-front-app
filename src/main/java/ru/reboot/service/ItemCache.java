package ru.reboot.service;

import org.springframework.stereotype.Component;
import ru.reboot.dto.ItemDTO;
import ru.reboot.error.BusinessLogicException;
import ru.reboot.error.ErrorCode;

import java.util.*;

/**
 * Item cache. Loads on startup
 */
@Component
public class ItemCache {

    private final Map<String, ItemDTO> items = new HashMap<>();

    /**
     * Get item by item id.
     *
     * @param itemId - item id
     * @return item or null if item doesn't exists
     */
    public ItemDTO get(String itemId) {
        validateItemId(itemId);
        if (items.isEmpty()) {
            return null;
        }
        ItemDTO item;
        item = items.get(itemId);
        return item;
    }

    public void put(String key, ItemDTO value) {
        validateItemId(key);
        this.items.put(key, value);
    }

    public void delete(String key) {
        validateItemId(key);
        items.remove(key);
    }

    public boolean contains(String itemId) {
        validateItemId(itemId);
        return items.containsKey(itemId);
    }

    private void validateItemId(String itemId) {
        if (Objects.isNull(itemId) || Objects.equals(itemId, "")) {
            throw new BusinessLogicException("itemId is null or empty", ErrorCode.ILLEGAL_ARGUMENT);
        }
    }

    public List<ItemDTO> values() {
        return new ArrayList<>(items.values());
    }
}
