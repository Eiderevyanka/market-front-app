package ru.reboot.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.reboot.dto.CategoryInfo;
import ru.reboot.dto.ItemDTO;
import ru.reboot.dto.ItemInfo;
import ru.reboot.dto.Order;
import ru.reboot.error.BusinessLogicException;
import ru.reboot.service.MarketService;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Price controller.
 */
@RestController
@RequestMapping(path = "market")
public class MarketControllerImpl implements MarketController {

    private static final Logger logger = LogManager.getLogger(MarketControllerImpl.class);

    private MarketService marketService;

    @Autowired
    public void setMarketService(MarketService marketService) {
        this.marketService = marketService;
    }

    @GetMapping("info")
    public String info() {
        logger.info("method .info invoked");
        return "MarketController " + new Date();
    }

    @Override
    @GetMapping("category/all")
    public List<CategoryInfo> getCategories() {
        return marketService.getCategories();
    }

    @Override
    @GetMapping("item/all/byCategory")
    public List<ItemInfo> getItemsByCategory(@RequestParam("categoryId") String category) {
        return marketService.getItemsByCategory(category);
    }

    @PostMapping("order")
    public Order createOrder (@RequestBody Order order) throws Exception {
        return marketService.createOrder(order);
    }

    @DeleteMapping("order")
    public void deleteOrder(@RequestParam("orderId") String orderId) throws Exception {
        marketService.deleteOrder(orderId);
    }

    @GetMapping("orders/byPhoneNumber")
    public List<Order> getOrdersByPhoneNumber(@RequestParam("phoneNumber") String phoneNumber) throws Exception {
        return marketService.getOrdersByPhoneNumber(phoneNumber);
    }

    @ExceptionHandler(BusinessLogicException.class)
    public ResponseEntity<Map<String, String>> handleException(BusinessLogicException ex, HttpServletRequest request) {

        HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
        Map<String, String> map = new HashMap<>();

        map.put("errorCode", ex.getCode());
        map.put("message", ex.getMessage());
        map.put("timestamp", LocalDateTime.now().toString());
        map.put("status", String.valueOf(status.value()));
        map.put("error", status.getReasonPhrase());
        map.put("path", request.getRequestURI());

        return new ResponseEntity<>(map, status);
    }
}
