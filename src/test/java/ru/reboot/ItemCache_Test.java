package ru.reboot;

import org.junit.Assert;
import org.junit.Test;
import ru.reboot.dto.ItemDTO;
import ru.reboot.error.BusinessLogicException;
import ru.reboot.service.ItemCache;


public class ItemCache_Test {

    @Test
    public void get_Test() {
        ItemCache items = new ItemCache();

        ItemDTO item = new ItemDTO.Builder().setId("Milk").build();
        items.put("Milk", item);

        Assert.assertEquals(item, items.get("Milk"));
        Assert.assertEquals(null, items.get("Bread"));
    }

    @Test
    public void put_Test() {
        ItemCache items = new ItemCache();

        ItemDTO itemFirst = new ItemDTO.Builder().setId("Milk").build();
        ItemDTO itemSecond = new ItemDTO.Builder().setId("Bread").build();
        items.put("Milk", itemFirst);
        items.put("Bread", itemSecond);

        Assert.assertEquals(itemFirst, items.get("Milk"));
        Assert.assertEquals(itemSecond, items.get("Bread"));
    }

    @Test
    public void contains_Test() {
        ItemCache items = new ItemCache();

        ItemDTO itemFirst = new ItemDTO.Builder().setId("Milk").build();
        ItemDTO itemSecond = new ItemDTO.Builder().setId("Bread").build();
        items.put("Milk", itemFirst);
        items.put("Bread", itemSecond);

        Assert.assertTrue(items.contains("Milk"));
        Assert.assertTrue(items.contains("Bread"));
        Assert.assertFalse(items.contains("Apple"));
    }

    @Test
    public void delete_Test() {
        ItemCache items = new ItemCache();

        ItemDTO itemFirst = new ItemDTO.Builder().setId("Milk").build();
        ItemDTO itemSecond = new ItemDTO.Builder().setId("Bread").build();

        items.put("Milk", itemFirst);
        items.put("Bread", itemSecond);
        Assert.assertTrue(items.contains("Milk"));
        Assert.assertTrue(items.contains("Bread"));

        items.delete("Milk");
        Assert.assertFalse(items.contains("Milk"));

        items.delete("Bread");
        Assert.assertFalse(items.contains("Bread"));
    }

    @Test
    public void allMethods_whenItemIdIsNull_Test() {
        ItemCache items = new ItemCache();

        try {
            items.get(null);
            Assert.fail();
        } catch (BusinessLogicException ex) {
            Assert.assertTrue(true);
        }

        try {
            items.put(null, new ItemDTO());
            Assert.fail();
        } catch (BusinessLogicException ex) {
            Assert.assertTrue(true);
        }

        try {
            items.contains(null);
            Assert.fail();
        } catch (BusinessLogicException ex) {
            Assert.assertTrue(true);
        }

        try {
            items.delete(null);
            Assert.fail();
        } catch (BusinessLogicException ex) {
            Assert.assertTrue(true);
        }
    }
}
