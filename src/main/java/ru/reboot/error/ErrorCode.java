package ru.reboot.error;

public class ErrorCode {

    public static final String INITIALIZE_ITEM_CACHE_ERROR = "INITIALIZE_ITEM_CACHE_ERROR";
    public static final String INITIALIZE_PRICE_CACHE_ERROR = "INITIALIZE_PRICE_CACHE_ERROR";

    public static final String ITEM_NOT_FOUND = "ITEM_NOT_FOUND";
    public static final String ITEM_ALREADY_EXISTS = "ITEM_ALREADY_EXISTS";
    public static final String RESERVATION_ERROR = "RESERVATION_ERROR";
    public static final String ILLEGAL_ARGUMENT = "ILLEGAL_ARGUMENT";
    public static final String PRICE_NOT_FOUND = "PRICE_NOT_FOUND";
    public static final String DATABASE_ERROR = "DATABASE_ERROR";
    public static final String DUPLICATE_LOGIN = "DUPLICATE_LOGIN";
    public static final String DUPLICATE_USERID = "DUPLICATE_USERID";
    public static final String USER_NOT_FOUND = "USER_NOT_FOUND";
    public static final String CANT_CREATE_NEW_USER = "CANT_CREATE_NEW_USER";
    public static final String CANT_UPDATE_USER = "CANT_CREATE_NEW_USER";
    public static final String ORDER_NOT_FOUND = "ORDER_NOT_FOUND";
    public static final String CANT_CREATE_NEW_ORDER = "CANT_CREATE_NEW_ORDER";
    public static final String DUPLICATE_ORDER = "DUPLICATE_ORDER";
}
