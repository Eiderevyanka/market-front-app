package ru.reboot.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;
import ru.reboot.dto.*;
import ru.reboot.error.BusinessLogicException;
import ru.reboot.error.ErrorCode;

import javax.annotation.PostConstruct;
import java.net.URL;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class MarketService {

    private static final Logger logger = LogManager.getLogger(MarketService.class);

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Value("${client.stock-service}")
    private String stockServiceUrl;

    @Value("${client.price-service}")
    private String priceServiceUrl;

    @Value("${client.order-service}")
    private String orderServiceUrl;

    private ObjectMapper mapper;
    private ItemCache itemCache;
    private PriceCache priceCache;
    private RestTemplate restTemplate;

    @Autowired
    public void setMapper(ObjectMapper mapper) {
        this.mapper = mapper;
    }

    @Autowired
    public void setItemCache(ItemCache itemCache) {
        this.itemCache = itemCache;
    }

    @Autowired
    public void setPriceCache(PriceCache priceCache) {
        this.priceCache = priceCache;
    }

    @Autowired
    public void setRestTemplate(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @KafkaListener(topics = "ITEM_CHANGED_EVENT", groupId = "market-front-app", autoStartup = "${kafka.autoStartup}")
    public void onItemChangedEventListener(String raw) {

        logger.info("Method .onItemChangedEventListener");
        logger.info("<< Received: {}", raw);
        try {
            ItemChangedEvent event = mapper.readValue(raw, ItemChangedEvent.class);
            if (event.getEventType().equals(ItemChangedEvent.Type.DELETE)) {
                itemCache.delete(event.getItem().getId());
                logger.info("Method .onItemChangedEventListener completed, delete item={}", event.getItem());
            } else {
                itemCache.put(event.getItem().getId(), event.getItem());
                logger.info("Method .onItemChangedEventListener completed, put item={}", event.getItem());
            }
        } catch (Exception e) {
            logger.error("Failed to .onItemChangedEventListener error={}", e.toString(), e);
        }
    }

    @KafkaListener(topics = "PRICE_CHANGED_EVENT", groupId = "market-front-app", autoStartup = "${kafka.autoStartup}")
    public void onPriceChangedEventListener(String raw) {

        logger.info("Method .onPriceChangedEventListener");
        logger.info("<< Received: {}", raw);
        try {
            PriceChangedEvent event = mapper.readValue(raw, PriceChangedEvent.class);
            if (event.getEventType().equals(ItemChangedEvent.Type.DELETE)) {
                priceCache.delete(event.getPrice().getItemId());
                logger.info("Method .onPriceChangedEventListener completed, delete price={}", event.getPrice());
            } else {
                priceCache.put(event.getPrice().getItemId(), event.getPrice());
                logger.info("Method .onPriceChangedEventListener completed, put price={}", event.getPrice());
            }
        } catch (Exception e) {
            logger.error("Failed to .onPriceChangedEventListener error={}", e.toString(), e);
        }
    }

    /**
     * Get available categories.
     * Returns empty collection if no category found.
     */
    public List<CategoryInfo> getCategories() {

        logger.info("Method .getCategories");

        List<CategoryInfo> categories = new ArrayList<>();

        itemCache.values()
                .stream()
                .map(ItemDTO::getCategory)
                .distinct()
                .forEach(s -> categories.add(new CategoryInfo(s)));

        logger.info("Method .getCategories completed result={}", categories);
        return categories;
    }

    /**
     * Get all items by category with assigned price.
     */
    public List<ItemInfo> getItemsByCategory(String category) {

        logger.info("Method .getItemsByCategory");

        List<ItemInfo> items = itemCache.values()
                .stream()
                .filter(item -> item.getCategory().equalsIgnoreCase(category))
                .filter(item -> priceCache.contains(item.getId()))
                .map(item -> new ItemInfo(item, priceCache.get(item.getId())))
                .collect(Collectors.toList());

        logger.info("Method .getItemsByCategory completed result={}", items);
        return items;
    }

    /**
     * create Order
     */
    public Order createOrder(Order order) throws Exception {

        try {
            logger.info("Method .createOrder");

            order.getItems().forEach((item, count) -> {
                ItemDTO itemDTO = itemCache.get(item);
                if ((itemDTO.getCount() - itemDTO.getReserved()) < count) {
                    throw new BusinessLogicException("Товар " + itemDTO.getShortName() +
                            " отсутствует на складе в необходимом количестве", ErrorCode.RESERVATION_ERROR);
                }
            });

            order.setOrderDate(LocalDateTime.now());
            String json = mapper.writeValueAsString(order);
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            Order createdOrder;

            try {
                createdOrder = restTemplate.postForObject(orderServiceUrl + "/orders/order",
                        new HttpEntity<>(json, headers), Order.class);
            } catch (HttpServerErrorException.InternalServerError ex) {
                Map<String, String> map = mapper.readValue(ex.getResponseBodyAsString(), Map.class);
                if (map.containsKey("errorCode")) {
                    throw new BusinessLogicException(map.get("message"), map.get("errorCode"));
                } else {
                    throw ex;
                }
            }

            logger.info("Method .createOrder completed, Order={}", createdOrder);
            return createdOrder;

        } catch (Exception ex) {
            logger.error("Failed to .createOrder error={}", ex.toString(), ex);
            throw ex;
        }
    }

    /**
     * delete Order
     */
    public void deleteOrder(String orderId) throws Exception {

        try {
            logger.info("Method .deleteOrder");

            try {
                restTemplate.delete(orderServiceUrl + "/orders/order?orderId=" + orderId);
            } catch (HttpServerErrorException.InternalServerError ex) {
                Map<String, String> map = mapper.readValue(ex.getResponseBodyAsString(), Map.class);
                if (map.containsKey("errorCode")) {
                    throw new BusinessLogicException(map.get("message"), map.get("errorCode"));
                } else {
                    throw ex;
                }
            }

            logger.info("Method .deleteOrder completed");

        } catch (Exception ex) {
            logger.error("Failed to .deleteOrder error={}", ex.toString(), ex);
            throw ex;
        }
    }

    /**
     * Get all orders by phone number
     */
    public List<Order> getOrdersByPhoneNumber(String phoneNumber) throws Exception {

        try {
            logger.info("Method .getOrdersByPhoneNumber");

            String url = orderServiceUrl + "/orders/all/byPhoneNumber?phoneNumber=";
            Order[] orders;

            try {
                orders = restTemplate.getForObject(url + phoneNumber, Order[].class);
            } catch (HttpServerErrorException.InternalServerError ex) {
                Map<String, String> map = mapper.readValue(ex.getResponseBodyAsString(), Map.class);
                if (map.containsKey("errorCode")) {
                    throw new BusinessLogicException(map.get("message"), map.get("errorCode"));
                } else {
                    throw ex;
                }
            }

            List<Order> orderList = Arrays.asList(Objects.requireNonNull(orders));

            logger.info("Method .getOrdersByPhoneNumber completed, Order[]={}", orderList);
            return orderList;

        } catch (Exception ex) {
            logger.error("Failed to .getOrdersByPhoneNumber error={}", ex.toString(), ex);
            throw ex;
        }
    }

    /**
     * Init cache here.
     */
    @PostConstruct
    public void init() {
        initializePriceCache();
        initializeItemCache();
    }

    /**
     * Init Price cache here.
     */
    private void initializePriceCache() {

        logger.info("Method .initializePriceCache started");

        RestTemplate restTemplate = new RestTemplate();

        try {
            PriceInfo[] prices = restTemplate.getForObject(priceServiceUrl + "/price/all", PriceInfo[].class);

            for (PriceInfo priceInfo : prices) {
                priceCache.put(priceInfo.getItemId(), priceInfo);
            }

            logger.info("Method .initializePriceCache completed, itemCount={}", prices.length);

        } catch (Exception e) {
            logger.error("Failed to .initializePriceCache error={}", e.toString(), e);
            throw new BusinessLogicException("Method .initializePriceCache failed ", ErrorCode.INITIALIZE_PRICE_CACHE_ERROR);
        }
    }

    private void initializeItemCache() {
        logger.info("Method .initializeItemCache started");
        try {
            List<ItemDTO> list = Arrays.asList(mapper.readValue(new URL(stockServiceUrl + "/stock/item/all"), ItemDTO[].class));
            list.forEach(s -> itemCache.put(s.getId(), s));
            logger.info("Method .initializeItemCache completed, itemCount={}", list.size());
        } catch (Exception ex) {
            logger.error("Failed to .initializeItemCache error={}", ex.toString(), ex);
            throw new BusinessLogicException(ex.getMessage(), ErrorCode.INITIALIZE_ITEM_CACHE_ERROR);
        }
    }
}
