package ru.reboot.service;

import org.junit.Test;
import org.mockito.Mockito;
import ru.reboot.dto.CategoryInfo;
import ru.reboot.dto.ItemDTO;

import java.util.*;

import static org.junit.Assert.*;

public class GetCategoriesTest {

    private final ItemCache itemCache = Mockito.mock(ItemCache.class);
    private final MarketService marketService = new MarketService();

    {
        marketService.setItemCache(itemCache);
    }

    @Test
    public void getCategoriesPositive() {

        Mockito.when(itemCache.values()).thenReturn(Arrays.asList(
                new ItemDTO.Builder().setCategory("fruits").build(),
                new ItemDTO.Builder().setCategory("fruits").build(),
                new ItemDTO.Builder().setCategory("qwerty").build(),
                new ItemDTO.Builder().setCategory("qwerty").build(),
                new ItemDTO.Builder().setCategory("vegetables").build()));

        List<CategoryInfo> categoryInfos = marketService.getCategories();

        Mockito.verify(itemCache).values();
        assertEquals(categoryInfos.size(), 3);
        assertEquals(categoryInfos.get(0).getId(), "fruits");
        assertEquals(categoryInfos.get(1).getId(), "qwerty");
        assertEquals(categoryInfos.get(2).getId(), "vegetables");
        assertEquals(categoryInfos.get(0).getName(), "UNKNOWN_CATEGORY_NAME (ID=fruits)");
        assertEquals(categoryInfos.get(1).getName(), "UNKNOWN_CATEGORY_NAME (ID=qwerty)");
        assertEquals(categoryInfos.get(2).getName(), "UNKNOWN_CATEGORY_NAME (ID=vegetables)");
    }

    @Test
    public void getCategoriesEmptyList() {

        Mockito.when(itemCache.values()).thenReturn(Collections.emptyList());

        List<CategoryInfo> categoryInfos = marketService.getCategories();

        Mockito.verify(itemCache).values();
        assertEquals(categoryInfos.size(), 0);
    }
}