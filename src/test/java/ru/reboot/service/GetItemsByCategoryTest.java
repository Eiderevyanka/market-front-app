package ru.reboot.service;


import org.junit.Test;
import org.mockito.Mockito;
import ru.reboot.dto.ItemDTO;
import ru.reboot.dto.ItemInfo;
import ru.reboot.dto.PriceInfo;

import java.util.*;

import static org.junit.Assert.*;

public class GetItemsByCategoryTest {

    private final ItemCache itemCache = Mockito.mock(ItemCache.class);
    private final PriceCache priceCache = Mockito.mock(PriceCache.class);
    private final MarketService marketService = new MarketService();

    {
        marketService.setItemCache(itemCache);
        marketService.setPriceCache(priceCache);
    }

    @Test
    public void getItemsByCategoryPositive() {

        Mockito.when(itemCache.values()).thenReturn(Arrays.asList(
                new ItemDTO.Builder().setCategory("fruits").setId("1").build(),
                new ItemDTO.Builder().setCategory("fruits").setId("2").build(),
                new ItemDTO.Builder().setCategory("qwerty").setId("3").build(),
                new ItemDTO.Builder().setCategory("qwerty").setId("4").build(),
                new ItemDTO.Builder().setCategory("fruits").setId("5").build(),
                new ItemDTO.Builder().setCategory("vegetables").setId("6").build()));

        Mockito.when(priceCache.values()).thenReturn(Arrays.asList(
                new PriceInfo("1", 10.5),
                new PriceInfo("2", 15.5),
                new PriceInfo("3", 50.5),
                new PriceInfo("4", 100.1)));

        Mockito.when(priceCache.contains(Mockito.anyString())).thenReturn(true);
        Mockito.when(priceCache.contains("5")).thenReturn(false);

        Mockito.when(priceCache.get("1")).thenReturn(10.5);
        Mockito.when(priceCache.get("2")).thenReturn(15.5);

        List<ItemInfo> itemInfos = marketService.getItemsByCategory("fruits");

        assertEquals(itemInfos.size(), 2);
        assertEquals(itemInfos.get(0).getPrice(), 10.5, 0);
        assertEquals(itemInfos.get(0).getId(), "1");
        assertEquals(itemInfos.get(1).getPrice(), 15.5, 0);
        assertEquals(itemInfos.get(1).getId(), "2");
    }

    @Test
    public void getItemsByCategoryEmptyList() {

        Mockito.when(itemCache.values()).thenReturn(Collections.emptyList());
        Mockito.when(priceCache.values()).thenReturn(Arrays.asList(
                new PriceInfo("1", 10.5),
                new PriceInfo("2", 15.5),
                new PriceInfo("3", 50.5),
                new PriceInfo("4", 100.1)));

        List<ItemInfo> itemInfos1 = marketService.getItemsByCategory("fruits");

        Mockito.when(itemCache.values()).thenReturn(Arrays.asList(
                new ItemDTO.Builder().setCategory("fruits").setId("1").build(),
                new ItemDTO.Builder().setCategory("fruits").setId("2").build(),
                new ItemDTO.Builder().setCategory("qwerty").setId("3").build(),
                new ItemDTO.Builder().setCategory("qwerty").setId("4").build(),
                new ItemDTO.Builder().setCategory("fruits").setId("5").build(),
                new ItemDTO.Builder().setCategory("vegetables").setId("6").build()));
        Mockito.when(priceCache.values()).thenReturn(Collections.emptyList());

        Mockito.when(priceCache.contains(Mockito.anyString())).thenReturn(false);

        List<ItemInfo> itemInfos2 = marketService.getItemsByCategory("fruits");

        Mockito.verify(itemCache, Mockito.times(2)).values();
        assertEquals(itemInfos1, itemInfos2);
        assertEquals(itemInfos1, Collections.emptyList());
    }
}
