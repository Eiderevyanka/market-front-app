package ru.reboot.service;

import org.springframework.stereotype.Component;
import ru.reboot.dto.PriceInfo;
import ru.reboot.error.BusinessLogicException;
import ru.reboot.error.ErrorCode;

import java.util.*;

/**
 * Item cache. Loads on startup.
 */
@Component
public class PriceCache {

    private final Map<String, PriceInfo> priceCache = new HashMap<>();

    /**
     * Get item by item id.
     *
     * @param itemId - item id
     * @return item or null if item doesn't exists
     */
    public Double get(String itemId) {
        if (contains(itemId)) {
            return priceCache.get(itemId).getPrice();
        }
        return null;
    }

    /**
     * Check if item has assigned price.
     *
     * @param itemId - item id
     */
    public boolean contains(String itemId) {
        validateItemId(itemId);
        if (priceCache.containsKey(itemId)) {
            return true;
        }
        return false;
    }

    public List<PriceInfo> values() {
        return new ArrayList<>(priceCache.values());
    }

    /**
     * Put item by cache.
     *
     * @param itemId    - item id
     * @param priceInfo
     */
    public void put(String itemId, PriceInfo priceInfo) {
        priceCache.put(itemId, priceInfo);
    }

    /**
     * Delete item by cache.
     *
     * @param itemId - item id
     */
    public void delete(String itemId) {
        if (contains(itemId)) {
            priceCache.remove(itemId);
        }
    }

    /**
     * Check if itemId is null or empty.
     *
     * @param itemId - item id
     * @throws {@link BusinessLogicException} with code ILLEGAL_ARGUMENT if itemId is null or empty
     */
    private void validateItemId(String itemId) {
        if (Objects.isNull(itemId) || itemId.equals("")) {
            throw new BusinessLogicException("itemId can't be null", ErrorCode.ILLEGAL_ARGUMENT);
        }
    }

}